
#ifndef INCLUDES_H
#define INCLUDES_H

		//---LAS LIBRERIAS---
#include <stdio.h>
#include <windows.h>

#include "imagen.h"
#include "graphics.h"
#include "ebentoak.h"
#include "text.h"
#include "soinua.h"

#include <SDL.h>
#include <SDL_joystick.h>		 //--> para ponerle mandos de xbox
#include <SDL_video.h>		//--> para a�adir video.
#include <SDL_thread.h>  //
#include <SDL_timer.h>  //


	//---JOKOAN ZEHAR SORTUTAKO ELEMENTUEN LIBURUTEGIAK---

	#include "jokalaria.h"






		//---IRUDIAK---

#define JOKOA_PLAYER_IMAGE ".\\img\\invader.bmp"
#define BUKAERA_IMAGE ".\\img\\gameOver_1.bmp"
#define MENUAREN_IRUDIA ".\\img\\portada.bmp"
#define KREDITUEN_IRUDIA ".\\img\\kredituak.bmp"
#define HASIERAKO_IRUDIA ".\\img\\hasierakoa.bmp"
#define DENDA ".\\img\\denda.bmp"


		//---EL SONIDO---

#define MENUAREN_SOINUA ".\\sound\\menu.wav"

#define PUNTUAZIOAK ".\\sound\\ranking.wav"

#define EASY_MODE_MUSIC ".\\sound\\easy_mode.wav"
#define JOKOA_SOUND_WIN ".\\sound\\kredituak.wav"
#define MUERTE ".\\sound\\dead.wav" 
#define BUKAERA_SOUND_1 ".\\sound\\dead.wav"


		//---EL TEXTO---

#define ONGI_ETORRI_MEZUA "Sakatu return hasteko..."



		//---VIDEOA---
#define OPENING ".\\video\\OPENING.mp4"





#endif // !HEADER_H
