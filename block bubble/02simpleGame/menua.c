
#include "game02.h"


int main()
{


	//OpenFile(OPENING);



	EGOERA egoera;

	if (sgHasieratu() == -1)
	{
		fprintf(stderr, "Unable to set 640x480 video: %s\n", SDL_GetError());
		return 1;
	}


	//videoa emen

	SDL_Init(SDL_INIT_JOYSTICK);

	SDL_Joystick* joystick = SDL_JoystickOpen(0);

	if (SDL_NumJoysticks() < 1)
	{
		SDL_Log("%d number of controlees are connected", 0);
	}
	
	



	textuaGaitu();

	int fondoa = irudiaKargatu(HASIERAKO_IRUDIA);
	


	while (ebentuaJasoGertatuBada() != TECLA_RETURN)
	{
		pantailaGarbitu();
		irudiakMarraztu();
		pantailaBerriztu();
	}

	irudiaKendu(fondoa);


	//MENUA

	hasieratu_menua();

	int ebentu = 0, irten = 0;
	POSIZIOA pos;


	while (!irten)
	{


		while (1)
		{
			pantailaGarbitu();
			irudiakMarraztu();


			ebentu = ebentuaJasoGertatuBada();


			if (ebentu == SAGU_BOTOIA_EZKERRA)
			{

				pos = saguarenPosizioa();

				if ((pos.x > 290) && (pos.x < 405) && (pos.y > 130) && (pos.y < 145))//PLAY botoiari dagokio
				{
					menua_itxi();
					egoera = jokatu();
					
				}
				else if ((pos.x > 264) && (pos.x < 379) && (pos.y > 232) && (pos.y < 250))// RANKING dagokio
				{
					menua_itxi();
					vistaratu_ranking();
					//Ranking-a egin txt a ikusteko soilik.
				}
				if ((pos.x > 264) && (pos.x < 379) && (pos.y > 179) && (pos.y < 197))//TUTORIAL botoiari dagokio
				{

					menua_itxi();

					while (1)
					{
						pantailaGarbitu();
						irudiakMarraztu();

						if (ebentuaJasoGertatuBada() == TECLA_ESCAPE)
						{
							irudiaKendu(fondoa);
							break;
						}


						//Bideoa jarri


						pantailaBerriztu();
					}

				}
				else if ((pos.x >288) && (pos.x < 350) && (pos.y > 394) && (pos.y < 412))//EXIT botoiari dagokio
				{

					irudiaKendu(fondoa);
					break;
				}
				else if ((pos.x > 264) && (pos.x < 379) && (pos.y > 282) && (pos.y < 300))//CREDITS botoiari dagokio
				{
					menua_itxi();
					credits();
				}

			}
			else if (ebentu == TECLA_ESCAPE)
			{
				irten = 1;
			}
			pantailaBerriztu();

		}
		audioTerminate();
		sgItxi();
		return 0;
	}
}

void hasieratu_menua(void)
{
	audioInit();
	loadTheMusic(MENUAREN_SOINUA);
	playMusic();

	int Menuaren_fondoa = irudiaKargatu(MENUAREN_IRUDIA);
}

void menua_itxi()
{
	irudiaKendu(MENUAREN_IRUDIA);
	audioTerminate();

}




void sarreraMezuaIdatzi()
{
	pantailaGarbitu();
	textuaIdatzi(225, 200, ONGI_ETORRI_MEZUA);
	pantailaBerriztu();
}



void credits()
{
	int Kredituen_Fondoa = irudiaKargatu(KREDITUEN_IRUDIA);
	irudiaMugitu(KREDITUEN_IRUDIA, 0, 0);
	pantailaGarbitu();
	irudiakMarraztu();
	pantailaBerriztu();

	while (1)
	{
		if (ebentuaJasoGertatuBada() == TECLA_ESCAPE)
		{
			irudiaKendu(KREDITUEN_IRUDIA);
			break;
		}
	
	

	}

	hasieratu_menua();

}


void vistaratu_ranking()
{
	RANKING zerrenda[10];

	int i = 0;

	char zenbaki_textifikatuak[1];

	POSIZIOA rank;

	rank.x = 75,rank.y=40;



	audioInit();

	loadTheMusic(PUNTUAZIOAK);

	playMusic();

	ranking_zerrenda_kargatu(zerrenda);


	pantailaGarbitu();

	//emen idatziak sartzen dira

	//textuak idatzi funtzioak ezin dituenez int aldagaiak idatzi char etara pasatzen ditugu.

	//nota zenbakiak ez dira ondo idazten.

	do
	{
		zenbaki_textifikatuak[0]=conversion_int_to_char(zerrenda[i].posizioa,zenbaki_textifikatuak);
		
		textuaIdatzi(rank.x+100,rank.y, zenbaki_textifikatuak);
		
		textuaIdatzi(rank.x+200, rank.y, zerrenda[i].jokalariaren_izena);


		zenbaki_textifikatuak[0] = conversion_int_to_char(zerrenda[i].zonbie_kills,zenbaki_textifikatuak);

		textuaIdatzi(rank.x+300, rank.y, zenbaki_textifikatuak);

		rank.y = rank.y + 40;
		
		i++;

	} while (i<=9);
	
	
	pantailaBerriztu();

	while (1)
	{
		if (ebentuaJasoGertatuBada() == TECLA_ESCAPE)
		{

			break;
		}

	}

	audioTerminate();

	hasieratu_menua();

}

char conversion_int_to_char(int zenbakia)
{
	int i = 0;
	int a = zenbakia;
	char chara= zenbakia;

	return chara;
}