
#define _CRT_SECURE_NO_WARNINGS
#include "game02.h"


EGOERA jokatu(void) 
{
  int mugitu = 0; //boolean
  int ebentu = 0;
  
  int zonbie_kills=51;

  int dirua = 0;

  JOKO_ELEMENTUA zirkulua, jokalaria;
  POSIZIOA aux;
  EGOERA  egoera = JOLASTEN;


  zirkulua.pos.x = 200;
  zirkulua.pos.y = 200;
  //Uint32 time01 = SDL_GetTicks(), time02;

  jokalaria.pos.x = 320;
  jokalaria.pos.y = 220;

  audioInit();
  loadTheMusic(EASY_MODE_MUSIC);
  playMusic();
  jokalaria.id = JOKOA_jokalariaIrudiaSortu();

  do {

		Sleep(2);
		aux = ERREALITATE_FISIKOA_mugimendua(zirkulua.pos);
		zirkulua.pos.y = aux.y;
		pantailaGarbitu();
		arkatzKoloreaEzarri(0, 0, 0xFF);
		zirkuluaMarraztu(zirkulua.pos.x, zirkulua.pos.y, 20);
		irudiaMugitu(jokalaria.id, jokalaria.pos.x, jokalaria.pos.y);
		irudiakMarraztu();
		pantailaBerriztu();


		ebentu = ebentuaJasoGertatuBada();


		jokalaria = jokalariaren_mugimenduak(ebentu, jokalaria,dirua);
		

		pausaMenua(dirua,ebentu);

		//zonbie bat akabatzean player.zonbie_kills++; jarri

		//zonbie bat akabatzean dirua gehitu

		if (zirkulua.pos.y + 30 > 478) {
		  zirkulua.pos.y = 0 + 30;
		}

		if (mugitu) {
		  aux = ERREALITATE_FISIKOA_mugimendua(jokalaria.pos);
		  jokalaria.pos.x = aux.x;
		}

		egoera = JOKOA_egoera(jokalaria, zirkulua);

  } while (egoera == JOLASTEN);

  irudiaKendu(jokalaria.id);
  toggleMusic();
  audioTerminate();
  pantailaGarbitu();
  pantailaBerriztu();



  Jokoa_itxi(egoera,zonbie_kills);

  return egoera;
}


EGOERA JOKOA_egoera(JOKO_ELEMENTUA jokalaria, JOKO_ELEMENTUA oztopoa) {
  EGOERA  ret = JOLASTEN;
  if (jokalaria.pos.x >oztopoa.pos.x - 20 && jokalaria.pos.x <oztopoa.pos.x + 20 && jokalaria.pos.y >oztopoa.pos.y - 20 && jokalaria.pos.y <oztopoa.pos.y + 20) {
    ret = IRABAZI;
  }
  else if (jokalaria.pos.x > 600) {
    ret = GALDU;
  }
  return ret;
}


POSIZIOA ERREALITATE_FISIKOA_mugimendua(POSIZIOA posizioa) {
  posizioa.y = posizioa.y + 1;
  posizioa.x = posizioa.x + 1;
  return posizioa;
}


int  Jokoa_itxi(EGOERA egoera,int zonbie_kils)
{
  int ebentu = 0, id;
  int idAudioGame;

  //loadTheMusic(BUKAERA_SOUND_1);

  if (egoera == IRABAZI) {
    idAudioGame = loadSound(MUERTE);
    playSound(idAudioGame);
  }
  else {
    idAudioGame = loadSound(JOKOA_SOUND_WIN);
    playSound(idAudioGame);
  }

  id=BUKAERA_irudiaBistaratu();


//aqui va el ranking


  puntuazioa_rankinean_sartu(zonbie_kils);


  do 
  {
    ebentu = ebentuaJasoGertatuBada();

  } while ((ebentu!= TECLA_RETURN) && (ebentu!= SAGU_BOTOIA_ESKUMA));


  audioTerminate();
  irudiaKendu(id);


  //menua hasieratu egiten dugu itxi baino lehen

  	audioInit();
	loadTheMusic(MENUAREN_SOINUA);
	playMusic();

	int Menuaren_fondoa = irudiaKargatu(MENUAREN_IRUDIA);
	//irudiaMugitu(fondoa, 0, 0);



  return (ebentu != TECLA_RETURN) ? 1 : 0;
}

int BUKAERA_irudiaBistaratu() {
  int id = -1;
  id = irudiaKargatu(BUKAERA_IMAGE);
  irudiaMugitu(id, 200, 200);
  pantailaGarbitu();
  irudiakMarraztu();
  pantailaBerriztu();
  return id;
}


void puntuazioa_rankinean_sartu(int zonbie_kils)
{
	char mezua[128] = {" Sartu jokalariaren izena:"};

	mezua[strlen(mezua) - 1] = "\0";
	
	RANKING gure_jokalaria;

	RANKING zerrenda[10];

	RANKING tmp;

	FILE *fp;
	int chara;
	int i = 0;


	//izena modu interaktiboan sartzeko atala FALTA DA


	/*
	textuaGaitu();

	textuaIdatzi(200, 25, "Sartu jokalariaren izena:");

	textuaDesgaitu();
	*/

	strcpy(gure_jokalaria.jokalariaren_izena, "emen_sartu_izena");

	gure_jokalaria.zonbie_kills = zonbie_kils;

	

	ranking_zerrenda_kargatu(zerrenda);

	
		//fitxategiko elementuak gure jokalariarekin konparatzen ditugu. eta ordenean jarri lehenengo 10 bakarrik

		do
		{
			if (gure_jokalaria.zonbie_kills > zerrenda[i].zonbie_kills)
			{
				gure_jokalaria.posizioa = zerrenda[i].posizioa;

				tmp=zerrenda[i];
				zerrenda[i] = gure_jokalaria;
				
				gure_jokalaria = tmp;

			}
			else
			{
				i++;
			}
			

		} while (i<=9);

		//orain ordenatutako arraia fitxerora sartzen dugu.

		i = 0;

		fp=fopen("ranking.txt", "w");

		do
		{
			
			chara = fprintf(fp, "%d %s %d\n", zerrenda[i].posizioa
											, zerrenda[i].jokalariaren_izena,
											  zerrenda[i].zonbie_kills);

			i++;

			

		} while (i<=9);


	fclose(fp);	

}


RANKING* ranking_zerrenda_kargatu(RANKING ranking_jokalariak[])
{
	
	unsigned int i = 0;

	FILE *fp;
	int chara=0;

	//fitxategia irekitzen dugu aldaketak gauzatzeko

	fp = fopen("ranking.txt", "r");

	if (fp == NULL)
	{
		printf("\n \n ez da kargatu");
	}
	else
	{
		//fitxategiko elementu guztira struktura batetara sartzen ditugu konparatzeko

		while ((chara != -1) && (i < 10))
		{

			chara = fscanf(fp, "%d %s %d \n", &ranking_jokalariak[i].posizioa
											,ranking_jokalariak[i].jokalariaren_izena,
											&ranking_jokalariak[i].zonbie_kills);

			i++;

		} 


		fclose(fp);

	}

	return ranking_jokalariak;
}