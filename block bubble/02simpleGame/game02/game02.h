#ifndef GAME10_H
#define GAME10_H

#include "ourTypes.h"

#include "includes.h"



//MENURAKO ERABILTZEN DIRENAK
void sarreraMezuaIdatzi();
void hasieratu_menua(void);
void menua_itxi();
void credits();
void vistaratu_ranking();
char conversion_int_to_char(int zenbakia, char gordailua[]);


//void zirkuluaBistaratu(int xRef, int yRef);
void JOKOA_lerroHBatMargotu(int x, int y, int x1, int y1);
void JOKOA_lerroVBatMargotu(int x, int y, int x1, int y1);



//JOKOAN ZEHARREKOAK

EGOERA jokatu(void);
int  Jokoa_itxi(EGOERA egoera, int zonbie_kils);
EGOERA JOKOA_egoera(JOKO_ELEMENTUA jokalaria, JOKO_ELEMENTUA oztopoa);
void pausaMenua(int dirua);

POSIZIOA ERREALITATE_FISIKOA_mugimendua(POSIZIOA posizioa);
int BUKAERA_irudiaBistaratu();


void puntuazioa_rankinean_sartu(int zonbie_kils);
RANKING* ranking_zerrenda_kargatu(RANKING ranking_jokalariak[]);





//int  BUKAERA_menua(EGOERA egoera);


#endif